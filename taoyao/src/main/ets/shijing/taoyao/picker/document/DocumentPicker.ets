import { Document } from './Document';
import  { picker } from '@kit.CoreFileKit';
import { BusinessError } from '@kit.BasicServicesKit';
import { DocumentBuilder } from './DocumentBuilder';
import { Origin } from '../../origin/Origin';
import { TextUtils } from '../../utils/TextUtils';
import { deviceInfo } from '@kit.BasicServicesKit';
import { common } from '@kit.AbilityKit';

export class DocumentPicker implements Document {

  private origin: Origin
  private mOnSuccess: ((uri: Array<string>) => void) | null = null
  private mOnError: ((error: BusinessError) => void) | null = null

  constructor(origin: Origin) {
    this.origin = origin
  }

  select(documentBuilder: DocumentBuilder): Document {
    const documentSelectOptions = new picker.DocumentSelectOptions();
    // 选择文档的最大数目（可选）
    documentSelectOptions.maxSelectNumber = documentBuilder.maxSelectNumber;
    // 指定选择的文件或者目录路径（可选）
    if (!TextUtils.isEmpty(documentBuilder.defaultFilePathUri)) {
      // file://docs/storage/Users/currentUser/test
      documentSelectOptions.defaultFilePathUri = documentBuilder.defaultFilePathUri
    }
    // 选择文件的后缀类型['后缀类型描述|后缀类型']（可选） 若选择项存在多个后缀名，则每一个后缀名之间用英文逗号进行分隔（可选），后缀类型名不能超过100,选择所有文件：'所有文件(*.*)|.*';
    documentSelectOptions.fileSuffixFilters = documentBuilder.fileSuffixFilters

    if (canIUse("SystemCapability.FileManagement.UserFileService.FolderSelection") && deviceInfo.sdkApiVersion >= 12) {
      //选择是否对指定文件或目录授权，true为授权，当为true时，defaultFilePathUri为必选参数，拉起文管授权界面；false为非授权，拉起常规文管界面（可选）
      documentSelectOptions.authMode = documentBuilder.authMode;
      if (documentBuilder.authMode) {
        if (TextUtils.isEmpty(documentBuilder.defaultFilePathUri)) {
          throw new Error('当为true时，defaultFilePathUri为必选参数')
        }
      }
    }
    // 创建文件选择器实例
    const documentViewPicker = new picker.DocumentViewPicker(this.origin.getContext());
    documentViewPicker.select(documentSelectOptions).then((documentSelectResult: Array<string>) => {
      //文件选择成功后，返回被选中文档的uri结果集。
      this.mOnSuccess?.(documentSelectResult)
    }).catch((err: BusinessError) => {
      this.mOnError?.(err)
    })
		return this
	}

  onSuccess(onSuccess: (uri: Array<string>) => void): Document {
    this.mOnSuccess = onSuccess
    return this
  }

  onError(onError: (error: BusinessError) => void): Document {
    this.mOnError = onError
    return this
  }

}